<?php
namespace Drupal\server_sent_events;

class ServerSentEventsService{

  public function getChannelPath($channel){
    $workDir = \Drupal::service('file_system')->realpath('private://').'/server_sent_events/';
    $this->prepareFolder($workDir);
    return $workDir.$channel;
  }

  public function prepareFolders($channelFolder,$requestId){
    $uid = \Drupal::currentUser()->id();
    $this->prepareFolder($channelFolder);
    $this->prepareFolder($channelFolder.'/' .$uid);
    $this->prepareFolder($channelFolder.'/' .$uid .'/'.$requestId);
  }

  public function getChannelSubscribers($channel){
    $channelFolder = $this->getChannelPath($this->channel);
    $subscribers = [];
    foreach(scandir($channelFolder) as $dir){
      if($dir != '.'&& $dir !='..' && is_dir($channelFolder.'/' .$dir)){
        $subscribers[] = $dir;
      }
    }
    return $subscribers;
  }

  public function prepareFolder($folder){
    if(!file_exists($folder) || !is_dir($folder)){
      mkdir($folder);
    }
  }
}
