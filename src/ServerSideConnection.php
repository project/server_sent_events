<?php
namespace Drupal\server_sent_events;

class ServerSideConnection{
  public function __construct($channel){
    $this->channel = $channel;
    $this->requestId = $this->subscribe();
  }
  public function __destruct(){
    $this->unsubscribe();
  }
  public function send($data){
    echo $data;
    ob_flush();
    flush();
    if(connection_aborted()){
      $this->unsubscribe();
      exit();
    }
  }
  public function subscribe(){
    $channelFolder = \Drupal::service('server_sent_events.service')->getChannelPath($this->channel);
    $requestId = sprintf("%08x", abs(crc32($_SERVER['REMOTE_ADDR'] . $_SERVER['REQUEST_TIME'] . $_SERVER['REMOTE_PORT'])));
    \Drupal::service('server_sent_events.service')->prepareFolders($channelFolder,$requestId);
    return $requestId;
  }

  public function unsubscribe(){
    $channelFolder = \Drupal::service('server_sent_events.service')->getChannelPath($this->channel);
    $uid = \Drupal::currentUser()->id();
    $this->rrmdir($channelFolder.'/'. $uid.'/'.$this->requestId);
    if(count(scandir($channelFolder.'/' . $uid)) == 2){
      rmdir($channelFolder.'/'.$uid);
    }
    if(count(scandir($channelFolder)) == 2){
      rmdir($channelFolder);
    }
  }

  public function rrmdir($dir) {
    if (is_dir($dir)) {
      $objects = scandir($dir);
      foreach ($objects as $object) {
        if ($object != "." && $object != "..") {
          if (filetype($dir."/".$object) == "dir")
            $this->rrmdir($dir."/".$object);
          else unlink   ($dir."/".$object);
        }
      }
      reset($objects);
      rmdir($dir);
    }
  }

  public function getPendingEvents(){
    $uid = \Drupal::currentUser()->id();
    $channelFolder = \Drupal::service('server_sent_events.service')->getChannelPath($this->channel).'/'. $uid.'/'.$this->requestId;
    if(!file_exists($channelFolder) || !is_dir($channelFolder)){
      return [];
    }
    $files = [];
    foreach(scandir($channelFolder) as $file){
      if(is_file($channelFolder.'/' .$file)){
        $files[] = $channelFolder.'/' .$file;
      }
    }
    return $files;
  }

}
