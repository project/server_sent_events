<?php

namespace Drupal\server_sent_events\Event;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * A server sent event that will be pushed.
 */
class ServerSentEvent extends Event {

  const SERVER_SENT_EVENT = 'server_sent_event_dispatch';

  public function __construct($channel , $event_type = 'message' , $event_id = 0, $event_data = '' , $recipients = 'All') {
    $this->channel = $channel;
    $this->type = $event_type;
    $this->id = $event_id;
    $this->data = $event_data;
    $this->recipients = $recipients;
  }

  public function dispatch(){
    if($this->recipients === 'All'){
      $this->pushEvent(
        $this->channel,
        $this->id,
        $this->data,
        $this->type
      );
    }
    elseif(is_array($this->recipients)){
      foreach($this->recipients as $uid){
        $this->pushUserEvent(
          $this->channel,
          $this->id,
          $this->data,
          $uid,
          $this->type
        );
      }
    }
    else{
      $this->pushUserEvent(
        $this->channel,
        $this->id,
        $this->data,
        $this->recipients,
        $this->type
      );
    }
    \Drupal::service('event_dispatcher')->dispatch(ServerSentEvent::SERVER_SENT_EVENT, $this);
  }

  public function pushUserEvent($channel , $id , $data , $uid , $eventType){
    $event = "event: $eventType\nid: $id\ndata: $data\n\n";
    $channelFolder = \Drupal::service('server_sent_events.service')->getChannelPath($channel);
    \Drupal::service('server_sent_events.service')->prepareFolder($channelFolder);
    \Drupal::service('server_sent_events.service')->prepareFolder($channelFolder . '/' .$uid);
    $fileName = time();
    foreach(scandir($channelFolder . '/' .$uid) as $dir){
      if($dir != '.'&& $dir !='..' && is_dir($channelFolder.'/'.$uid.'/' .$dir)){
        $fileHandle = fopen($channelFolder.'/' .$uid.'/'.$dir.'/'.$fileName,'a+');
        fwrite($fileHandle,$event);
        fclose($fileHandle);
      }
    }
  }

  public function pushEvent($channel,$id, $data,$eventType){
    $event = "event: $eventType\nid: $id\ndata: $data\n\n";
    $channelFolder = \Drupal::service('server_sent_events.service')->getChannelPath($channel);
    \Drupal::service('server_sent_events.service')->prepareFolder($channelFolder);
    $fileName = time();
    foreach(scandir($channelFolder) as $dir){
      if($dir != '.'&& $dir !='..' && is_dir($channelFolder.'/' .$dir)){
        foreach(scandir($channelFolder.'/' .$dir) as $subscriber){
          if($subscriber != '.'&& $subscriber !='..' && is_dir($channelFolder.'/' .$dir.'/'.$subscriber)){
            $fileHandle = fopen($channelFolder.'/' .$dir.'/'.$subscriber.'/'.$fileName,'a+');
            fwrite($fileHandle,$event);
            fclose($fileHandle);
          }
        }
      }
    }
  }

}
