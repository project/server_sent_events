<?php

namespace Drupal\server_sent_events\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\server_sent_events\Event\ServerSentEvent;
use Drupal\server_sent_events\ServerSideConnection;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\StreamedResponse;

/**
 * Returns responses for Server Sent Events routes.
 */
class ServerSentEventsController extends ControllerBase {
  /**
   * Builds the response.
   */
  public function channel($channel) {
    ignore_user_abort(1);
    $response = new StreamedResponse(function() use($channel){
      $ssc = new ServerSideConnection($channel);
      $event = new ServerSentEvent($channel,'subscribed',\Drupal::currentUser()->id(),"Successfully subscribed to channel $channel");
      $event->dispatch();
      $x = 0;
      while (TRUE) {
        $eventFiles = $ssc->getPendingEvents();
        foreach ($eventFiles as $eventFile) {
          $eventFileHandle = fopen($eventFile, "r") or die("Can't open event file");
          $ssc->send(fread($eventFileHandle, filesize($eventFile)));
          fclose($eventFileHandle);
          unlink($eventFile);
        }
        //Replace 5 with configurable setting
        if($x % 5 == 0){
          $ssc->send("\n");
        }
        $x++;
        sleep(1);
      }
    });
    $response->headers->set('Content-Type', 'text/event-stream');
    $response->send();
  }

  public function pushMessage($channel){
    $data = $_POST['data'];
    $event = new ServerSentEvent($channel,$_POST['eventType'],\Drupal::currentUser()->id(),$data);
    $event->dispatch();
    return new JsonResponse(1);
  }


}
