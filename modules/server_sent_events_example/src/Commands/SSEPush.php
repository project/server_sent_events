<?php

namespace Drupal\server_sent_events_example\Commands;
use Drupal\server_sent_events\Event\ServerSentEvent;
use Drush\Commands\DrushCommands;
/**
 * A drush command file.
 *
 * @package Drupal\server_sent_events_example\Commands
 */
class SSEPush extends DrushCommands {
  /**
   * Drush command that deploys AG settings.
   *
   * @command server_sent_events_example:push
   * @aliases ssep
   */
  public function push($channel,$event_type,$event_data,$event_recipient = 'All') {
    $event = new ServerSentEvent(
      $channel,
      $event_type,
      'Drush push',
      $event_data,
      $event_recipient
    );
    $event->dispatch();
  }
}
