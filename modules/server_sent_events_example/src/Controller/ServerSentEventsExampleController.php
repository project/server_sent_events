<?php

namespace Drupal\server_sent_events_example\Controller;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;

class ServerSentEventsExampleController extends ControllerBase {
  public function simpleChatPage(){
    $response['chat_box'] = [
      '#type' => 'markup',
      '#markup' => '
        <table border="1">
          <tr>
            <th>Chat Channel</th>
            <th>Participants</th>
          </tr>
          <tr>
            <td class="chat-box"></td>
            <td class="chat-participants"></td>
          </tr>
          <tr>
            <td>
              <a class="button btn chat-ping">Ping</a>
              <a class="button btn chat-custom-action">Custom Action</a>
            </td>
          </tr>
        </table>
      '
    ];
    $response['message_box'] = [
      '#type' => 'textfield',
      '#title' => 'Type something',
      '#suffix' => '<a class="button btn chat-send">Submit</a>',
      '#attributes' =>['class' => ['chat-message']]
    ];
    $response['actions'] = [
      '#type'=>'container',
    ];
    $response['#attached']['library'][] = 'server_sent_events_example/server_sent_events_example';
    return $response;
  }

  public function getChannelParticipants($channel){
    $participants = \Drupal::service('server_sent_events.service')->getChannelSubscribers($channel);
    return new JsonResponse($participants);
  }
}
