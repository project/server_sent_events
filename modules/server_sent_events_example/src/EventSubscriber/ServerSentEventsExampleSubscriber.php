<?php
namespace Drupal\server_sent_events_example\EventSubscriber;

use Drupal\server_sent_events\Event\ServerSentEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class UpdateSecretarySchoolModeratorSubscriber.
 *
 * @package Drupal\ag_groups\EventSubscriber
 */
class ServerSentEventsExampleSubscriber implements EventSubscriberInterface {
  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      ServerSentEvent::SERVER_SENT_EVENT => 'onServerSentEvent',
    ];
  }

  /**
   * React to the add moderator event dispatched.
   *
   * @param \Drupal\server_sent_events\Event\ServerSentEvent $event
   */
  public function onServerSentEvent(ServerSentEvent $event) {
    \Drupal::logger('server_sent_events_example')->notice(
      "Server sent event type {$event->type} triggered\n
      Event id: {$event->id}\n
      Event data: {$event->data}\n
      Event Channel: {$event->channel}\n
      Event recipients: " . json_encode($event->recipients)
    );
  }

}
