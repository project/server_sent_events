(function($, Drupal) {
  console.log('Example js running');
  var serverSideChannel = new ServerSideChannel(1);

  serverSideChannel.eventSource.onmessage = function(message) {
    console.log('Message event' ,message);
    $('.chat-box').append('<div><span>User #'+message.lastEventId+':</span><span>'+message.data+'</span></div>');
  }
  serverSideChannel.eventSource.addEventListener("ping", function(event) {
    console.log("Ping event: " , event);
    $('.chat-box').append('<div><span>User #'+event.lastEventId+':</span><span>>Sent a Ping '+event.data+'</span></div>');
  });
  serverSideChannel.eventSource.addEventListener("custom-action", function(event) {
    console.log("Custom Action event: " , event);
    $('.chat-box').append('<div><span>User #'+event.lastEventId+':</span><span>>Sent a custom action '+event.data+'</span></div>');
  });
  serverSideChannel.eventSource.addEventListener("subscribed", function(event) {
    console.log("Subscribed event" , event);
    $('.chat-box').append('<div><span>User #'+event.lastEventId+' :</span><span>'+event.data+'</span></div>');
    var url = '/server-sent-events-example/participants/1';
    $.get(url).done(function(response){
      console.log('Refreshed chat participants',response);
      $('.chat-participants').html('');
      for(x in response){
        var user = '<div>User #'+response[x]+'</div>';
        $('.chat-participants').append(user);
      }
    })
  });

  $('.chat-ping').click(function(){
    console.log("Sending Ping event");
    serverSideChannel.push('','ping');
  });
  $('.chat-custom-action').click(function(){
    console.log("Sending Custom action event");
    serverSideChannel.push('','custom-action');
  });
  $('.chat-send').click(function(){
    console.log("Sending Message event");
    serverSideChannel.push($('.chat-message').val());
    $('.chat-message').val('');
  });

})(jQuery, Drupal)
