  class ServerSideChannel{
    constructor(channel) {
      this.channel = channel;
      //get path from drupalsettings
      this.eventSource =  new EventSource('/server-sent-events/' + channel);
      var me = this;
      this.eventSource.addEventListener('subscribed',function(event){
        me.requestId = event.lastEventId;
      });
    }
    push(message,event ='message'){
      var url = '/server-sent-events/push/' + this.channel;
      /*var xhr = new XMLHttpRequest();
      xhr.open("POST", url, true);
      xhr.setRequestHeader('Content-Type', 'application/json');
      xhr.send(JSON.stringify({
        data:message,
        requestId:this.requestId,
        eventType:event,
      }));*/
      jQuery.post(url, {data:message , requestId:this.requestId,eventType:event}).done(function(response){

      });
    }
  }

